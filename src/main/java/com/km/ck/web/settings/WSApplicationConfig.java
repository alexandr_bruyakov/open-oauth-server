package com.km.ck.web.settings;

/**
 * Created by ok on 24.07.14.
 */

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("/ws")
public class WSApplicationConfig extends ResourceConfig {
    public WSApplicationConfig() {
        super(MultiPartFeature.class);
        packages("com.km.ck.web.services");
    }
}