package ua.maximchuk.oauth.datamodel;

import com.j256.ormlite.jdbc.DataSourceConnectionSource;
import com.j256.ormlite.support.ConnectionSource;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.SQLException;

/**
 * @author Maxim Maximchuk
 *         date    8/8/14.
 */
public class DBHelper {

    private static final String JNDI_NAME = "java:comp/env/oauthDB";

    private ConnectionSource connectionSource;

    private static class SingletonHolder {
        private static DBHelper instance = new DBHelper();
    }

    public static DBHelper getInstance() {
        return SingletonHolder.instance;
    }

    private DBHelper() {
        try {
            DataSource ds = (DataSource) new InitialContext().lookup(JNDI_NAME);
            connectionSource = new DataSourceConnectionSource(ds, ds.getConnection().getMetaData().getURL());
        } catch (NamingException | SQLException ex) {
            ex.printStackTrace();
        }
    }

    public ConnectionSource getConnectionSource() {
        return connectionSource;
    }
}
