package ua.maximchuk.oauth.datamodel.dao;

import ua.maximchuk.oauth.datamodel.entity.UserEntity;

import java.sql.SQLException;

/**
 * @author Maxim Maximchuk
 *    date 06.10.2014.
 */
public class UserDao extends BaseDao<UserEntity, Long> {

    public UserDao() throws SQLException {
        super(UserEntity.class);
    }

    public UserEntity findUser(String username) throws SQLException {
        return queryBuilder().where().eq(UserEntity.USERNAME_FIELD, username).queryForFirst();
    }

}
