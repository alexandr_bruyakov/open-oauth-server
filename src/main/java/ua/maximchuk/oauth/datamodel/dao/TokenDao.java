package ua.maximchuk.oauth.datamodel.dao;

import com.j256.ormlite.stmt.QueryBuilder;
import ua.maximchuk.oauth.datamodel.entity.AuthInfoEntity;
import ua.maximchuk.oauth.datamodel.entity.TokenEntity;

import java.sql.SQLException;

/**
 * @author Maxim Maximchuk
 *         date 03.10.2014.
 */
public class TokenDao extends BaseDao<TokenEntity, Long> {

    public TokenDao() throws SQLException {
        super(TokenEntity.class);
    }

    public TokenEntity findAccessToken(String token) throws SQLException {
        return queryBuilder().where().eq(TokenEntity.TOKEN_FIELD, token)
                .and().eq(TokenEntity.TYPE_FIELD, TokenEntity.Type.ACCESS)
                .queryForFirst();
    }

    public TokenEntity findRefreshToken(String token) throws SQLException {
        return queryBuilder().where().eq(TokenEntity.TOKEN_FIELD, token)
                .and().eq(TokenEntity.TYPE_FIELD, TokenEntity.Type.REFRESH)
                .queryForFirst();
    }

    public TokenEntity findAccessTokenByRefreshToken(TokenEntity refreshToken) throws SQLException {
        AuthInfoDao authInfoDao = new AuthInfoDao();
        authInfoDao.refresh(refreshToken.getAuthInfo());
        QueryBuilder<AuthInfoEntity, Long> authInfoQb = authInfoDao.queryBuilder();
        authInfoQb.where().eq(AuthInfoEntity.ID_FIELD, refreshToken.getAuthInfo().getId());
        return queryBuilder().join(authInfoQb).where().eq(TokenEntity.TYPE_FIELD, TokenEntity.Type.ACCESS).queryForFirst();
    }
}
