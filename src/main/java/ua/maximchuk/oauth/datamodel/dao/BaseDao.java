package ua.maximchuk.oauth.datamodel.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import ua.maximchuk.oauth.datamodel.DBHelper;

import java.sql.SQLException;

/**
 * @author Maxim Maximchuk
 *         date 03.10.2014.
 */
public abstract class BaseDao<T, ID> extends BaseDaoImpl<T, ID> {

    protected BaseDao(Class<T> dataClass) throws SQLException {
        super(DBHelper.getInstance().getConnectionSource(), dataClass);
        initialize();
    }

}
