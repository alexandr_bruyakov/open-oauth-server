package ua.maximchuk.oauth.datamodel.dao;

import org.apache.oltu.oauth2.as.request.AbstractOAuthTokenRequest;
import ua.maximchuk.oauth.datamodel.entity.AuthCodeInfoEntity;
import ua.maximchuk.oauth.datamodel.entity.AuthInfoEntity;
import ua.maximchuk.oauth.datamodel.entity.TokenEntity;

import java.sql.SQLException;
import java.util.Date;

/**
 * @author Maxim Maximchuk
 *         date 03.10.2014.
 */
public class AuthInfoDao extends BaseDao<AuthInfoEntity, Long> {

    public AuthInfoDao() throws SQLException {
        super(AuthInfoEntity.class);
    }

    public AuthInfoEntity getAuthInfoByAccessToken(String token) throws SQLException {
        TokenDao tokenDao = new TokenDao();
        TokenEntity tokenEntity = tokenDao.findAccessToken(token);
        if (tokenEntity != null && tokenEntity.getExpires() * 1000 + tokenEntity.getCreateDate().getTime() < new Date().getTime()) {
            tokenDao.delete(tokenEntity);
            tokenEntity = null;
        }
        if (tokenEntity != null) {
            refresh(tokenEntity.getAuthInfo());
            return tokenEntity.getAuthInfo();
        } else {
            return null;
        }
    }

    public AuthInfoEntity getAuthInfoByRefreshToken(String token) throws SQLException {
        TokenDao tokenDao = new TokenDao();
        TokenEntity tokenEntity = tokenDao.findRefreshToken(token);
        if (tokenEntity != null) {
            refresh(tokenEntity.getAuthInfo());
            return tokenEntity.getAuthInfo();
        } else {
            return null;
        }
    }

    public AuthInfoEntity create(AuthCodeInfoEntity authCodeInfoEntity) throws SQLException {
        return createIfNotExists(new AuthInfoEntity(authCodeInfoEntity));
    }

    public AuthInfoEntity create(AbstractOAuthTokenRequest oauthRequest) throws SQLException {
        return createIfNotExists(new AuthInfoEntity(oauthRequest));
    }

}
