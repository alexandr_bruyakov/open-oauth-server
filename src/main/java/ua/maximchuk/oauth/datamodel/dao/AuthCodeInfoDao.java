package ua.maximchuk.oauth.datamodel.dao;


import ua.maximchuk.oauth.datamodel.entity.AuthCodeInfoEntity;

import java.sql.SQLException;

/**
 * @author Maxim Maximchuk
 *         date 19.12.2014.
 */
public class AuthCodeInfoDao extends BaseDao<AuthCodeInfoEntity, Long> {

    public AuthCodeInfoDao() throws SQLException {
        super(AuthCodeInfoEntity.class);
    }

    public AuthCodeInfoEntity getAuthInfoByCode(String authCode) throws SQLException {
        return queryBuilder().where().eq(AuthCodeInfoEntity.AUTH_CODE_FIELD, authCode).queryForFirst();
    }

}
