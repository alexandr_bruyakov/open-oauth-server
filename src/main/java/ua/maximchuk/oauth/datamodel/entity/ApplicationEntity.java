package ua.maximchuk.oauth.datamodel.entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.maximchuk.json.JsonDTO;
import com.maximchuk.json.annotation.JsonIgnore;

/**
 * @author Maxim Maximchuk
 *         date 03.10.2014.
 */
@DatabaseTable(tableName = "application")
public class ApplicationEntity extends JsonDTO implements IdEntity {

    public static final String CLIENT_ID_FIELD = "client_id";
    public static final String CLIENT_SECRET_FIELD = "client_secret";
    public static final String REDIRECT_URI_FIELD = "redirect_uri";

    @JsonIgnore
    @DatabaseField(generatedId = true)
    private Long id;

    @DatabaseField(columnName = CLIENT_ID_FIELD, canBeNull = false)
    private String clientId;

    @DatabaseField(columnName = CLIENT_SECRET_FIELD, canBeNull = false)
    private String clientSecret;

    @DatabaseField(columnName = REDIRECT_URI_FIELD, canBeNull = false)
    private String redirectUri;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getRedirectUri() {
        return redirectUri;
    }

    public void setRedirectUri(String redirectUri) {
        this.redirectUri = redirectUri;
    }
}
