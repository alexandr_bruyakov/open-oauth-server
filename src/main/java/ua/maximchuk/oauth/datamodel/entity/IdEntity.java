package ua.maximchuk.oauth.datamodel.entity;

/**
 * @author Maxim Maximchuk
 *         date 03.10.2014.
 */
public interface IdEntity {

    public static String ID_FIELD = "id";

    public Long getId();

}
