package ua.maximchuk.oauth;

import org.apache.oltu.oauth2.as.issuer.MD5Generator;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuer;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuerImpl;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import ua.maximchuk.oauth.datamodel.dao.TokenDao;
import ua.maximchuk.oauth.datamodel.entity.AuthInfoEntity;
import ua.maximchuk.oauth.datamodel.entity.TokenEntity;

import java.sql.SQLException;
import java.util.Date;

/**
 * @author Maxim Maximchuk
 *         date 02.10.2014.
 */
public class OAuthStoreIssuer extends OAuthIssuerImpl implements OAuthIssuer {

    public static final int EXPIRES = 3600;

    private AuthInfoEntity authInfo;

    public OAuthStoreIssuer() {
        super(new MD5Generator());
    }

    public OAuthStoreIssuer(AuthInfoEntity authInfo) {
        this();
        this.authInfo = authInfo;
    }


    @Override
    public String accessToken() throws OAuthSystemException {
        checkAuthInfo();
        String token = super.accessToken();

        try {
            TokenDao tokenDao = new TokenDao();

            TokenEntity tokenEntity = new TokenEntity();
            tokenEntity.setToken(token);
            tokenEntity.setCreateDate(new Date());
            tokenEntity.setType(TokenEntity.Type.ACCESS);
            tokenEntity.setExpires(EXPIRES);
            tokenEntity.setAuthInfo(authInfo);
            tokenDao.create(tokenEntity);
        } catch (SQLException e) {
            throw new OAuthSystemException(e);
        }
        return token;
    }

    @Override
    public String refreshToken() throws OAuthSystemException {
        checkAuthInfo();
        String token = super.refreshToken();

        try {
            TokenDao tokenDao = new TokenDao();

            TokenEntity tokenEntity = new TokenEntity();
            tokenEntity.setToken(token);
            tokenEntity.setCreateDate(new Date());
            tokenEntity.setType(TokenEntity.Type.REFRESH);
            tokenEntity.setAuthInfo(authInfo);
            tokenDao.create(tokenEntity);
        } catch (SQLException e) {
            throw new OAuthSystemException(e);
        }
        return token;
    }

    private void checkAuthInfo() throws OAuthSystemException {
        if (authInfo == null) {
            throw new OAuthSystemException("Default constructor unsupported this action");
        }
    }
}
