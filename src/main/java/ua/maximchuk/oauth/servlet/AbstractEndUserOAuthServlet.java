package ua.maximchuk.oauth.servlet;

import org.apache.oltu.oauth2.as.request.OAuthAuthzRequest;
import org.apache.oltu.oauth2.as.response.OAuthASResponse;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.OAuthResponse;
import ua.maximchuk.oauth.OAuthStoreIssuer;
import ua.maximchuk.oauth.datamodel.dao.ApplicationDao;
import ua.maximchuk.oauth.datamodel.dao.AuthCodeInfoDao;
import ua.maximchuk.oauth.datamodel.entity.AuthCodeInfoEntity;
import ua.maximchuk.oauth.datamodel.entity.UserEntity;
import ua.maximchuk.oauth.servlet.exception.HttpServletException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * @author Maxim Maximchuk
 *         date 14.01.2015.
 */
public abstract class AbstractEndUserOAuthServlet extends BaseOAuthServlet {

    public static final String USER_FORM_URL_PARAM = "user.form.url";


    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            OAuthAuthzRequest oauthRequest = new OAuthAuthzRequest(request);
            String redirectUri = getInitParameter(USER_FORM_URL_PARAM);
            if (redirectUri != null) {
                try {
                    validateRequest(oauthRequest);

                    AuthCodeInfoEntity authInfo = new AuthCodeInfoEntity();

                    try {
                        ApplicationDao appDao = new ApplicationDao();

                        authInfo.setApplication(appDao.findAppByClientId(oauthRequest.getClientId()));
                        authInfo.setAuthCode(new OAuthStoreIssuer().authorizationCode());

                        if (oauthRequest.getScopes().size() > 0) {
                            StringBuilder scopesBuilder = new StringBuilder();
                            for (String scope : oauthRequest.getScopes()) {
                                scopesBuilder.append(scope).append(" ");
                            }
                            scopesBuilder.deleteCharAt(scopesBuilder.length() - 1);
                            authInfo.setScopes(scopesBuilder.toString());
                        }

                        AuthCodeInfoDao dao = new AuthCodeInfoDao();
                        dao.create(authInfo);
                    } catch (SQLException e) {
                        throw new OAuthSystemException(e);
                    }

                    OAuthResponse resp = OAuthASResponse
                            .authorizationResponse(request, HttpServletResponse.SC_FOUND)
                            .setCode(authInfo.getAuthCode())
                            .location(redirectUri)
                            .buildQueryMessage();

                    response.sendRedirect(resp.getLocationUri());
                } catch (OAuthProblemException e) {
                    sendQueryError(response, e, redirectUri);
                }
            } else {
                error("Server does`t support this action");
            }
        } catch (OAuthProblemException e) {
            sendBodyError(response, e);
        } catch (OAuthSystemException | SQLException e) {
            e.printStackTrace();
            sendInternalError(response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            String code = request.getParameter("code");

            AuthCodeInfoEntity codeEntity;

            if (code != null) {
                AuthCodeInfoDao codeDao = new AuthCodeInfoDao();
                codeEntity = codeDao.getAuthInfoByCode(code);
                if (codeEntity != null && codeEntity.getUsername() == null) {
                    UserEntity user = getUser(username, password);
                    if (user == null || !user.getPassword().equals(password)) {
                        throw new HttpServletException("username or password is incorrect", HttpServletResponse.SC_UNAUTHORIZED);
                    }
                    codeEntity.setUsername(user.getUsername());
                    codeDao.update(codeEntity);
                } else {
                    throw new HttpServletException("code is invalid", HttpServletResponse.SC_UNAUTHORIZED);
                }
            } else {
                throw new HttpServletException("code does`t defined", HttpServletResponse.SC_BAD_REQUEST);
            }

            OAuthResponse resp = OAuthASResponse
                    .authorizationResponse(request, HttpServletResponse.SC_FOUND)
                    .setCode(codeEntity.getAuthCode())
                    .location(codeEntity.getApplication().getRedirectUri())
                    .buildQueryMessage();
            response.sendRedirect(resp.getLocationUri());
        } catch (HttpServletException e) {
            e.processResponse(response);
        } catch (Exception e) {
            e.printStackTrace();
            sendInternalError(response);
        }
    }

    protected abstract UserEntity getUser(String username, String password) throws Exception;

}
