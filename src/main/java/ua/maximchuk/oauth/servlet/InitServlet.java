package ua.maximchuk.oauth.servlet;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import org.apache.oltu.oauth2.as.issuer.MD5Generator;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import ua.maximchuk.oauth.datamodel.DBHelper;
import ua.maximchuk.oauth.datamodel.dao.*;
import ua.maximchuk.oauth.datamodel.entity.RootTokenEntity;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

/**
 * @author Maxim Maximchuk
 *         date 03.10.2014.
 */
public class InitServlet extends HttpServlet {

    private ConnectionSource cs;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        try {
            cs = DBHelper.getInstance().getConnectionSource();

            createTables();

            RootTokenDao dao = new RootTokenDao();
            if (!dao.isTableExists()) {
                createTable(dao);
                RootTokenEntity rootToken = new RootTokenEntity(new MD5Generator().generateValue());
                dao.create(rootToken);

                sendMessage(httpServletResponse, 201, rootToken.getToken());
            } else {
                sendMessage(httpServletResponse, 200, "Root already initialized");
            }
        } catch (SQLException | OAuthSystemException e) {
            e.printStackTrace();
        }
    }

    private void createTables() throws SQLException {
        createTable(new ApplicationDao());
        createTable(new UserDao());
        createTable(new AuthInfoDao());
        createTable(new AuthCodeInfoDao());
        createTable(new TokenDao());
    }

    private void sendMessage(HttpServletResponse response, int status, String msg) throws IOException {
        response.setStatus(status);
        response.addHeader("Content-type", "text/plain");
        PrintWriter printWriter = response.getWriter();
        printWriter.write(msg);
        printWriter.close();
    }

    private void createTable(Dao dao) throws SQLException {
        if (!dao.isTableExists()) {
            TableUtils.createTable(cs, dao.getDataClass());
        }
    }
}
