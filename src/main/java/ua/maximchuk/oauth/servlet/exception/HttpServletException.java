package ua.maximchuk.oauth.servlet.exception;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

/**
 * @author Maxim Maximchuk
 *         date 23.12.14.
 */
public class HttpServletException extends Exception {

    private int status;

    public HttpServletException(String message, int status) {
        super(message);
        this.status = status;
    }

    public void processResponse(HttpServletResponse response) throws IOException {
        response.setStatus(status);
        OutputStream os = null;
        try {
            os = response.getOutputStream();
            os.write(getMessage().getBytes());
        } finally {
            if (os != null) {
                os.close();
            }
        }

    }
}
