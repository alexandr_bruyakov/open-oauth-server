package ua.maximchuk.oauth.servlet.filter;

import ua.maximchuk.oauth.datamodel.dao.RootTokenDao;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * @author Maxim Maximchuk
 *         date 10.12.2014.
 */
public class RootAccessFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        String token = ((HttpServletRequest)servletRequest).getHeader("Authorization");
        boolean pass = false;
        if (token != null) {
            try {
                RootTokenDao dao = new RootTokenDao();
                pass = dao.checkToken(token);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (pass) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            ((HttpServletResponse)servletResponse).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }

    @Override
    public void destroy() {
    }
}
