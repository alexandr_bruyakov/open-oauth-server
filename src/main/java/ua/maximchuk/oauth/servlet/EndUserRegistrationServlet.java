package ua.maximchuk.oauth.servlet;

import ua.maximchuk.oauth.datamodel.dao.UserDao;
import ua.maximchuk.oauth.datamodel.entity.UserEntity;
import ua.maximchuk.oauth.servlet.exception.HttpServletException;

import javax.servlet.http.HttpServletResponse;

/**
 * @author Maxim Maximchuk
 *         date 14.01.2015.
 */
public class EndUserRegistrationServlet extends AbstractEndUserOAuthServlet {

    @Override
    protected UserEntity getUser(String username, String password) throws Exception {
        UserDao userDao = new UserDao();
        if (userDao.findUser(username) != null) {
            throw new HttpServletException("Username already exists", HttpServletResponse.SC_CONFLICT);
        }
        UserEntity user = new UserEntity();
        user.setUsername(username);
        user.setPassword(password);
        return userDao.createIfNotExists(user);
    }
}
