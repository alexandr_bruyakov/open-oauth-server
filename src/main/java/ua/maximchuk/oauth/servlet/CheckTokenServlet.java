package ua.maximchuk.oauth.servlet;

import com.maximchuk.json.exception.JsonException;
import ua.maximchuk.oauth.datamodel.dao.AuthInfoDao;
import ua.maximchuk.oauth.datamodel.entity.AuthInfoEntity;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * @author Maxim Maximchuk
 *         date 11.10.2014.
 */
public class CheckTokenServlet extends BaseOAuthServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String token = request.getParameter("token");
        if (token != null) {
            try {
                AuthInfoDao dao = new AuthInfoDao();
                AuthInfoEntity authInfo = dao.getAuthInfoByAccessToken(token);

                if (authInfo != null) {
                    sendBodyResponse(response, authInfo.toJSON());
                } else {
                    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                }
            } catch (SQLException | JsonException e) {
                e.printStackTrace();
                sendInternalError(response);
            }
        }
    }
}
