package ua.maximchuk.oauth.rest;

import org.json.JSONException;
import org.json.JSONObject;
import ua.maximchuk.oauth.datamodel.dao.ApplicationDao;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

/**
 * @author Maxim Maximchuk
 *         date 18.12.2014.
 */
@Path("application")
public class ApplicationController extends BaseController {

    @POST
    @Path("register")
    @Consumes("application/json")
    public Response register(JSONObject jsonObject) {
        try {
            String name = jsonObject.getString("name");
            String redirectUri = jsonObject.getString("redirectUri");

            ApplicationDao dao = new ApplicationDao();
            return Response.ok(dao.registerApplication(name, redirectUri).toJSON()).build();
        } catch (JSONException e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } catch (Exception e) {
            return internalError(e);
        }
    }

}
